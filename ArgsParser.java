////////////////////////////////////////////////////////////////////////////////
// ArgsParser.java
////////////////////////////////////////////////////////////////////////////////

package org.instructures;

import java.io.*;
import java.util.*;

// A general command-line argument parser following the Unix
// single-character option conventions (similar to getopt,
// http://en.wikipedia.org/wiki/Getopt) and also the GNU long-form
// option conventions (cf. getopt_long, ibid.).
//
// The API uses the fluent-interface style, as discussed in:
// http://www.martinfowler.com/bliki/FluentInterface.html.
public class ArgsParser {
	// Canned messages and formatting strings.
	private static final String DEFAULT_VERSION = "(unknown)",
			HELP_MESSAGE = "display this help and exit",
			VERSION_MESSAGE = "output version information and exit",
			GENERIC_OPTIONS = "OPTIONS",
			OPTION_SUMMARY_FMT = "%4s%s %-20s   %s%n";

	// Factory to make a new ArgsParser instance, to generate help
	// messages and to process and validate the arguments for a command
	// with the given `commandName`.
	public static ArgsParser create(String commandName) {

		return new ArgsParser(commandName);

	}

	// A queryable container to hold the parsed results.
	//
	// Options are added using on of the `optional`, `require`, and
	// `requireOneOf` methods. The presence of such Options in the
	// actual arguments processed can be queried via the `hasOption`
	// method.
	//
	// Operands can be associated with an Option or can stand
	// alone. Standalone Operands are added using the `requiredOperand`,
	// `optionalOperand`, `oneOrMoreOperands`, and `zeroOrMoreOperands`
	// methods. Operands associated with an Option are added when that
	// Option is added.
	public class Bindings {
		public boolean hasOption(Option optionToQuery) {
			return options.contains(optionToQuery);
		}

		// If an Operand is optional and has a default value, then this method
		// will return the default value when the Operand wasn't specified.
		public <T> T getOperand(Operand<T> operand) {
			if (operands.containsKey(operand)) {
				List<T> result = getOperands(operand);
				if (result.size() == 1) {
					return result.get(0);
				}
			} else if (operand.hasDefaultValue()) {
				return operand.getDefaultValue();
			}
			throw new RuntimeException(String.format(
					"Expected one binding for operand %s", operand));
		}

		public <T> List<T> getOperands(Operand<T> operand) {
			List<T> result = new ArrayList<>();
			if (operands.containsKey(operand)) {
				List<String> uninterpretedStrings = operands.get(operand);
				for (String stringFormat : uninterpretedStrings) {
					result.add(operand.convertArgument(stringFormat));
				}
			}
			return result;
		}

		private void addOption(Option option) {
			options.add(option);
		}

		private void bindOperand(Operand<?> operand, String lexeme) {
			List<String> bindings;
			if (operands.containsKey(operand)) {
				bindings = operands.get(operand);
			} else {
				bindings = new ArrayList<>();
				operands.put(operand, bindings);
			}
			try {
				operand.convertArgument(lexeme);
			} catch (Exception e) {
				
				throw new RuntimeException(String.format("(invalid format) %s",
						e.getMessage()));
					
			}
			bindings.add(lexeme);
		}

		private final Set<Option> options = new HashSet<>();
		private final Map<Operand<?>, List<String>> operands = new HashMap<>();

		private Bindings() {
			/* intentionally left blank */
		}
	}

	// Parses the given command-line argument values according to the
	// specifications set through calls to the `optional`, `require`,
	// `requireOneOf` and `operands` methods.
	//
	// When the given arguments don't match the options specified, an
	// error message is printed and the program exits.
	//
	// Options for displaying the help message and the version message
	// are supported by calls made to `help` and `version`,
	// respectively. A call to 'parse` will cause the program to exit if
	// the help or version options are present in the given `args`. If
	// both are specified, then both will be printed before exit.
	public ArgsParser.Bindings parse(String[] args) {

		Bindings bindings = new Bindings();

		boolean helpOrVersionExit = false;
		int indexOfOperand = 0;

		mainLoop: for (int i = 0; i < args.length; i++) {

			if (args[i].startsWith("-") && args[i].length() >= 2
					&& args[i].charAt(1) != '-') {
				// single letter option or has binding operand
				String parsedOption = args[i].substring(1);

				wordLoop: for (int k = 0; k < parsedOption.length(); k++) {

					String parsedVal = "" + parsedOption.charAt(k);

					if (optional.containsKey(parsedVal)) {
						Option toUse = optional.get(parsedVal);

						if (toUse.getSummary().equals(HELP_MESSAGE)) {

							printUsage(System.out);

							helpOrVersionExit = true;
						}

						if (toUse.getSummary().equals(VERSION_MESSAGE)) {
							System.out.println(versionString);
							helpOrVersionExit = true;
						}
						bindings.addOption(toUse);

						// check if operand bound

						if (toUse.hasOperand()) {
							Operand<?> temp = toUse.getOperand();

							// if its single char option
							if (parsedOption.length() == 1
									&& i + 1 < args.length) {

								i++;
								bindings.bindOperand(temp, args[i]);

							}
							// multichar arguments
							else if (parsedOption.length() > 1) {

								bindings.bindOperand(temp,
										parsedOption.substring(k + 1));

							}
							// does not have a operand, end of the list
							else {

								System.err
										.println("Error: Expected operand, but none found %n");
								printUsage(System.err);
								System.exit(1);
							}
							break wordLoop;

						}

						continue;
					}

					if (require.containsKey(parsedVal)) {
						Option toUse = require.get(parsedVal);

						bindings.addOption(toUse);

						containsReq.remove(toUse);

						if (toUse.hasOperand()) {

							Operand<?> temp = toUse.getOperand();

							// if its single char option
							if (parsedOption.length() == 1
									&& i + 1 < args.length) {

								i++;
								bindings.bindOperand(temp, args[i]);

							} else if (parsedOption.length() > 1) {

								bindings.bindOperand(temp,
										parsedOption.substring(k + 1));

							}

							else {
								System.err
										.println("Error: Expected operand, but none found %n");
								printUsage(System.err);
								System.exit(1);
							}
							break wordLoop;
						}

						continue;
					}

					for (List<Option> uniq : uniqOne) {
						for (int j = 0; j < uniq.size(); j++) {
							List<String> longFlags = new ArrayList<>();
							List<String> shortFlags = new ArrayList<>();
							uniq.get(j).getFlags(longFlags, shortFlags);
							if (shortFlags.contains(parsedVal)) {

								bindings.addOption(uniq.get(j));
								uniqCounter
										.put(uniq, uniqCounter.get(uniq) + 1);

								if (uniq.get(j).hasOperand()) {

									Operand<?> temp = uniq.get(j).getOperand();

									// if its single char option
									if (parsedOption.length() == 1
											&& i + 1 < args.length) {

										i++;
										bindings.bindOperand(temp, args[i]);

									} else if (parsedOption.length() > 1) {

										bindings.bindOperand(temp,
												parsedOption.substring(k + 1));

									}

									else {
										System.err
												.println("Error: Expected operand, but none found %n");
										printUsage(System.err);
										System.exit(1);
									}
									break wordLoop;

								}
								continue wordLoop;

							}
						}
					}
					// if not recognized
					System.err.println("Error: Unrecognized argument %n");
					this.printUsage(System.err);
					System.exit(1);
				}

			} else if (args[i].startsWith("--") && args[i].length() > 2) {
				String parsedVal = args[i].substring(2);
				// check if has operand
				int indexOfDelim = parsedVal.indexOf('=');
				String changedParsedVal = "";
				if (indexOfDelim != -1) {
					changedParsedVal = parsedVal.substring(0, indexOfDelim);
				}
				else{
					changedParsedVal=parsedVal;
				}

				if (optional.containsKey(changedParsedVal)) {
					Option toUse = optional.get(changedParsedVal);

					if (toUse.getSummary().equals(HELP_MESSAGE)) {
						if (summaryString != null) {
							System.out.println(summaryString);
						}
						helpOrVersionExit = true;
					}

					if (toUse.getSummary().equals(VERSION_MESSAGE)) {
						System.out.println(versionString);
						helpOrVersionExit = true;
					}
					bindings.addOption(toUse);

					// check if operand bound

					if (toUse.hasOperand()) {
						if (indexOfDelim != -1) {
							if (indexOfDelim != args[i].length() - 1) {
								Operand<?> temp = toUse.getOperand();
								bindings.bindOperand(temp, parsedVal.substring(
										indexOfDelim + 1, parsedVal.length()));

							} else {
								printUsage(System.err);
								System.exit(1);
							}
						} else if (i < args.length - 1) {
							i++;
							Operand<?> temp = toUse.getOperand();
							bindings.bindOperand(temp, args[i]);
						} else {
							System.err
									.println("Error: Expected operand, but none found %n");
							printUsage(System.err);
							System.exit(1);
						}
					}

					continue;

				}

				if (require.containsKey(changedParsedVal)) {
					Option toUse = require.get(changedParsedVal);

					bindings.addOption(toUse);
					containsReq.remove(toUse);
					
					if (toUse.hasOperand()) {
						if (indexOfDelim != -1) {
							if (indexOfDelim != args[i].length() - 1) {
								Operand<?> temp = toUse.getOperand();
								bindings.bindOperand(temp, parsedVal.substring(
										indexOfDelim + 1, parsedVal.length()));

							} else {
								printUsage(System.err);
								System.exit(1);
							}
						} else if (i < args.length - 1) {
							i++;
							Operand<?> temp = toUse.getOperand();
							bindings.bindOperand(temp, args[i]);
						} else {
							System.err
									.println("Error: Expected operand, but none found %n");
							printUsage(System.err);
							System.exit(1);
						}

					}
					continue;
				}

				for (List<Option> uniq : uniqOne) {

					for (int j = 0; j < uniq.size(); j++) {
						Option toUse = uniq.get(j);
						List<String> longFlags = new ArrayList<>();
						List<String> shortFlags = new ArrayList<>();
						toUse.getFlags(longFlags, shortFlags);

						if (longFlags.contains(changedParsedVal)) {
							bindings.addOption(uniq.get(i));
							uniqCounter.put(uniq, uniqCounter.get(uniq) + 1);
							if (toUse.hasOperand()) {
								if (indexOfDelim != -1) {
									if (indexOfDelim != args[i].length() - 1) {
										Operand<?> temp = toUse.getOperand();

										bindings.bindOperand(temp, parsedVal
												.substring(indexOfDelim + 1,
														parsedVal.length()));

									} else {
										printUsage(System.err);
										System.exit(1);
									}
								} else if (i < args.length - 1) {
									i++;
									Operand<?> temp = toUse.getOperand();
									bindings.bindOperand(temp, args[i]);
								} else {
									System.err
											.println("Error: Expected operand, but none found %n");
									printUsage(System.err);
									System.exit(1);
								}

							}
							continue mainLoop;
						}
						
					}

				}
				// if not recognized
				System.err.println("Error: unrecognized argument %n");
				this.printUsage(System.err);
				System.exit(1);
			}
			// is operand
			else {
				if (operandOrder.size() == 0) {
					
					System.err.printf("Error: Extra Operand Detected %n");
					printUsage(System.err);
				} else {
					bindings.bindOperand(operandOrder.get(0), args[i]);
					
					if (!allowsMult(operandOrder.get(0))) {
						operandOrder.remove(0);
					}

				}
			}

		}

		while (!operandOrder.isEmpty()) {
			Operand<?> op = operandOrder.remove(0);
			if (!isOptional(op)) {
				List<?> lexems = bindings.getOperands(op);
				if (lexems.size() < 1) {
					System.err.printf("Error: Missing required operand:%n");
					printUsage(System.err);
				}
			}
		}

		if (helpOrVersionExit == true) {
			System.exit(0);
		}

		for (int values : uniqCounter.values()) {

			if (values != 1) {
				System.err.println("Error: "+errStringUniq);
				printUsage(System.err);
				System.exit(1);
			}
		}

		if (containsReq.size() > 0) {
			System.err.println("Error: "+errStringReq);
			printUsage(System.err);
			System.exit(1);
		}

		return bindings;

	}

	// Uses the given `summaryString` when the help/usage message is printed.
	public ArgsParser summary(String summaryString) {
		this.summaryString = summaryString;
		return this;
	}

	// Enables the command to have an option to display the current
	// version, represented by the given `versionString`. The version
	// option is invoked whenever any of the given `flags` are used,
	// where `flags` is a comma-separated list of valid short- and
	// long-form flags.
	public ArgsParser versionNameAndFlags(String versionString, String flags) {
		this.versionString = versionString;
		this.versionOption = Option.create(flags).summary(VERSION_MESSAGE);
		return optional(versionOption);
	}

	// Enables an automated help message, generated from the options
	// specified. The help message will be invoked whenever any of the
	// given `flags` are used.
	//
	// The `flags` parameter is a comma-separated list of valid short-
	// and long-form flags, including the leading `-` and `--` marks.
	public ArgsParser helpFlags(String flags) {
		this.helpOption = Option.create(flags).summary(HELP_MESSAGE);
		return optional(helpOption);
	}

	// Adds the given option to the parsing sequence as an optional
	// option. If the option takes an Operand, the value of the
	// associated operand can be accessed using a reference to that
	// specific Operand instance.
	//
	// Throws an IllegalArgumentException if the given option specifies
	// flags that have already been added.
	public ArgsParser optional(Option optionalOption)
			throws IllegalArgumentException {

		// checking if exists in array of argument strings
		List<String> shortList1 = new ArrayList<String>();
		List<String> longList1 = new ArrayList<String>();
		optionalOption.getFlags(longList1, shortList1);
		for (String val : shortList1) {
			if (this.currentOptions.contains(val)) {
				throw new IllegalArgumentException("Repeated Flag");
			}
			currentOptions.add(val);
			optional.put(val, optionalOption);

		}
		for (String val : longList1) {
			if (this.currentOptions.contains(val)) {
				throw new IllegalArgumentException("Repeated Flag");
			}
			currentOptions.add(val);
			optional.put(val, optionalOption);
		}

		tempOption.add(optionalOption);

		return this;
	}

	// Adds the given option to the parsing sequence as a required
	// option. If the option is not present during argument parsing, an
	// error message is generated using the given `errString`. If the
	// option takes an Operand, the value of the associated operand can
	// be accessed using a reference to that specific Operand instance.
	//
	// Throws an IllegalArgumentException if the given option specifies
	// flags that have already been added.
	public ArgsParser require(String errString, Option requiredOption) {

		// checking if exists in array of argument strings
		List<String> shortList1 = new ArrayList<String>();
		List<String> longList1 = new ArrayList<String>();
		requiredOption.getFlags(longList1, shortList1);

		for (String val : shortList1) {
			if (this.currentOptions.contains(val)) {
				throw new IllegalArgumentException("Repeated Flag");

			}
			this.require.put(val, requiredOption);
			currentOptions.add(val);

		}
		for (String val : longList1) {
			if (this.currentOptions.contains(val)) {
				throw new IllegalArgumentException("Repeated Flag");

			}
			this.require.put(val, requiredOption);
			currentOptions.add(val);

		}

		this.containsReq.add(requiredOption);
		this.errStringReq = errString;
		tempOption.add(requiredOption);

		return this;
	}

	// Adds the given set of mutually-exclusive options to the parsing
	// sequence. An error message is generated using the given
	// `errString` when multiple options that are mutually exclusive
	// appear, and when none appear. An example of such a group of
	// mutually- exclusive options is when the option specifies a
	// particular mode for the command where none of the modes are
	// considered as a default.
	//
	// Throws an IllegalArgumentException if any of the given options
	// specify flags that have already been added.
	public ArgsParser requireOneOf(String errString, Option... exclusiveOptions) {
		// checking if exists in array of argument strings
		List<Option> temp = new ArrayList<Option>();

		for (int i = 0; i < exclusiveOptions.length; i++) {
			Option optionToCheck = exclusiveOptions[i];
			// check for repeats
			List<String> shortList1 = new ArrayList<String>();
			List<String> longList1 = new ArrayList<String>();
			optionToCheck.getFlags(longList1, shortList1);

			for (String val : shortList1) {
				if (this.currentOptions.contains(val)) {
					throw new IllegalArgumentException("Repeated Flag");
				}
				currentOptions.add(val);
			}
			for (String val : longList1) {
				if (this.currentOptions.contains(val)) {
					throw new IllegalArgumentException("Repeated Flag");
				}
				currentOptions.add(val);
			}
			temp.add(optionToCheck);
		}

		this.uniqCounter.put(temp, 0);
		this.errStringUniq = errString;
		this.uniqOne.add(Arrays.asList(exclusiveOptions));
		this.tempOption.addAll(Arrays.asList(exclusiveOptions));
		return this;
	}

	// Adds the given operand to the parsing sequence as a required
	// operand to appear exactly once. The matched argument's value is
	// retrievable from the `ArgsParser.Bindings` store by passing the
	// same `requiredOperand` instance to the `getOperand` method.
	public ArgsParser requiredOperand(Operand<?> requiredOperand) {
		this.handleOperation(requiredOperand, false, false);

		return this;
	}

	// Adds the given operand to the parsing sequence as an optional
	// operand. The matched argument's value is retrievable from the
	// `ArgsParser.Bindings` store by passing the same `optionalOperand`
	// instance to the `getOperands` method, which will return either a
	// the empty list or a list with a single element.
	public ArgsParser optionalOperand(Operand<?> optionalOperand) {
		this.handleOperation(optionalOperand, true, false);

		return this;
	}

	// Adds the given operand to the parsing sequence as a required
	// operand that must be specifed at least once and can be used
	// multiple times (the canonical example would be a list of one or
	// more input files).
	//
	// The values of the argumen matched is retrievable from the
	// `ArgsParser.Bindings` store by passing the same `operand`
	// instance to the `getOperands` method, which will return a list
	// with at least one element (should the arguments pass the
	// validation process).
	public ArgsParser oneOrMoreOperands(Operand<?> operand) {
		this.handleOperation(operand, false, true);

		return this;
	}

	// Adds the given operand to the parsing sequence as an optional
	// operand that can be used zero or more times (the canonical
	// example would be a list of input files, where if none are given
	// then stardard input is assumed).
	//
	// The values of the arguments matched is retrievable from the
	// `ArgsParser.Bindings` store by passing the same `operand`
	// instance to the `getOperands` method, which will return a list of
	// all matches, potentially the empty list.
	public ArgsParser zeroOrMoreOperands(Operand<?> operand) {
		this.handleOperation(operand, true, true);

		return this;
	}

	private final String commandName;

	private String summaryString = null;
	private String versionString = DEFAULT_VERSION;
	private Option helpOption = null;
	private Option versionOption = null;

	// contains all options

	private Map<Operand<?>, String> usage = new HashMap<Operand<?>, String>();
	private List<Operand<?>> operandOrder = new ArrayList<Operand<?>>();

	// list of all current options
	private List<Option> tempOption = new ArrayList<Option>();

	private List<List<Option>> uniqOne = new ArrayList<List<Option>>();
	private Map<List<Option>, Integer> uniqCounter = new HashMap<List<Option>, Integer>();
	private Map<String, Option> optional = new HashMap<String, Option>();;

	private Map<String, Option> require = new HashMap<String, Option>();
	private List<Option> containsReq = new ArrayList<Option>();

	private List<String> currentOptions = new ArrayList<String>();
	private String errStringReq;
	private String errStringUniq;

	private ArgsParser(String commandName) {
		this.commandName = commandName;
	}

	// TODO: Add more code here if you think it'll be helpful!
	private void printUsage(PrintStream p) {

		p.printf("Usage: java %s", this.commandName);

		String optStr = (currentOptions.size()==0) ? "" : "OPTIONS";
		if(tempOption.size()+containsReq.size()>1){
			optStr+="...";
		}
		if(tempOption.isEmpty()||containsReq.isEmpty()){
			optStr="["+optStr+"]";
		}
		p.printf(" %s ",optStr);
		for (String s : usage.values()) {
			p.printf(s + " ");
		}

		System.out.println("\n");
		for (Option temp : tempOption) {

			p.printf("%8s%s%20s%s%n", "", temp.toString(), "",
					temp.getSummary());
		}

	}

	private void handleOperation(Operand<?> op, boolean optional,
			boolean allowMult) {
		operandOrder.add(op);
		String txt = op.getDocName();
		if (allowMult) {
			txt = txt+"...";
		}
		if (optional) {
			txt = "["+txt+"]";
		}
		usage.put(op, txt);
	}

	private boolean isOptional(Operand<?> op) {
		return usage.get(op).startsWith("[");
	}

	private boolean allowsMult(Operand<?> op) {
		return usage.get(op).contains("...");
	}
}
